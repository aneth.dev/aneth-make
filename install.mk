# Standard Install locations
prefix        ?= /usr/local
exec_prefix   ?= ${prefix}
bindir        := ${exec_prefix}/bin
sbindir       := ${exec_prefix}/sbin
libdir        := ${exec_prefix}/lib
libexecdir    := ${exec_prefix}/libexec
infodir       := ${prefix}/info
datadir       := ${prefix}/share
sysconfdir    := ${prefix}/etc
localstatedir := ${prefix}/var
includedir    := ${prefix}/include

RM              := \rm -f
INSTALL         := \cp
LINK            := \ln -s
UNINSTALL       := $(RM)
MKDIR           := \mkdir --parent

# Targets
install: ${INSTALLS} ${SYMLINKS}
uninstall:
	$(UNINSTALL) ${INSTALLS} ${SYMLINKS}
.PHONY: install uninstall

# Rules
%/:                     ; $(MKDIR) $@
.PRECIOUS: %/

.SECONDEXPANSION:
${INSTALLS}: | $${@D}/; $(INSTALL) $^ $@
${SYMLINKS}: | $${@D}/; $(LINK)    $^ $@
