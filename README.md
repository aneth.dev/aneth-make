Æten-make
=========

dedicated-build-dir.mk
----------------------
Inclusion of this _makefile_ allows to build naturally into dedicated build directory. User can define `BUILDDIR_PREFIX`, `BUILDDIR_SUFFIX` and `BUILDDIR`. Default `BUILDDIR_PREFIX` is `build/` and `BUILDDIR` is defined as concatenation of `BUILDDIR_PREFIX` and `BUILDDIR_SUFFIX`.

TODO: add example.

install.mk
----------
Inclusion of this _makefile_ facilitates file installations. It adds install and uninstall phonies related of user defined INSTALLS SYMLINKS variable wich must indicated associated files.

TODO: add example.

gcc.mk
------
Generic makefile for c/c++ libraries or programs and manages headers as dedencies.

TODO: add example.

tree.mk
-------
Generates `*.mk` files from `*.tree` file.

TODO: document `*.tree` file format and add example.

java.mk
-------
Makefile for Java projects.

TODO: add example.

debian-repository.mk
--------------------
Debian custom repository creation. TODO document it.

TODO: add example.

adoc.mk
----
Generic makefile for asciidoc PDF generation with dblatex backend. It provides easy stylesheet customization. See tests/adoc/ directory for usage example.

TODO: add example.

ninja2make
----------
A Sed script which convert a Ninja build file to a Make file.
See [aneth-cli's configure script](https://gitlab.com/aneth.dev/aneth-cli/blob/master/configure) for a use case example.

